//Wait for code file
if (EDGE_RANK >= 0) {
    socket.on(`Process_${EDGE_RANK}`, (data) => {
        if (waitForResult.event == `Process_${EDGE_RANK}` && data.fromRank == waitForResult.fromRank) {
            waitForResult.result = data;
            waitForResult.event = '';
            waitForResult.arrived = true;
        }
        else {
            recvQueue.push(data);
            recvQueue.sort((a, b) => {
                b.time > a.time
            });
        }
    });
}

async function program_main() {
    const { foo } = await import('http://localhost:5000/test%20scripts/matrix_multiply.js');
}

program_main();
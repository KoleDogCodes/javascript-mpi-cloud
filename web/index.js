const express = require('express');
const app = express();
var cors = require('cors');

const port = process.env.PORT || 80;
const base = `${__dirname}/public`;

// app.use((req, res, next) => {
//     res.header("Access-Control-Allow-Origin", "*");
//     res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, REQUEST");
//     res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

//     next();
// });

app.use(cors());

// app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile(`${base}/controller.html`);
});

app.get('/node/:nodeid', (req, res) => {
    res.sendFile(`${base}/node.html`);
});

app.get('/:static', (req, res) => {
    const { static } = req.params;
    res.sendFile(`${base}/${static}`);
});

//Web Listener\\
app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
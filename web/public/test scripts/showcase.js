const ary = [1,2,3,4,5,6];

async function main() {
    const rank = MPI_Edge_Rank();

    if (rank == 0) {
        MPI_Send_Broadcast(ary);
        log("Sent broadcast to all nodes.");
    }
    else {
        const result  = await MPI_Recv_Broadcast();
        log(result);
    }
}

main();
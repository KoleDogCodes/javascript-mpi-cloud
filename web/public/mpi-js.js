//Connect to socket.io server
const socket = io('https://javascript-mpi-cloud-socket-3f99139c2f17.herokuapp.com/', {
    rememberUpgrade: true,
    transports: ['websocket'],
    secure: true,
    rejectUnauthorized: false
});

var waitForResult = { event: '', promise: undefined, arrived: false, fromRank: -1 };
var DeviceCount = -1;
var recvQueue = [];
const EDGE_RANK = parseInt(window.location.href.split('/')[window.location.href.split('/').length - 1]);

//Setup HTML
if (EDGE_RANK >= 0) {
    $('#title').text(`Node ${EDGE_RANK} Terminal`);
    $('title').text(`Node ${EDGE_RANK}`);
}

//Listen for client connection event
socket.on('connect', () => {
    $('#conn_status').attr('type', 'success');
    $('#conn_status').text('CONNECTED TO SERVER');
    //socket.emit('connection_status', 'ping');

    log('Successfully connected to the server.');
});

//Listen for client disconnection event
socket.on('disconnect', () => {
    $('#conn_status').attr('type', 'failed');
    $('#conn_status').text('DISCONNECTED FROM SERVER');
    log('Disconnected from the server.');
});

//Listen for edge count from server
socket.on('Device_Count', (data) => {
    DeviceCount = data;

    if (waitForResult.event == 'Device_Count') {
        DeviceCount = data;
        waitForResult.arrived = true;
        waitForResult.result = data;

        waitForResult.event = '';
    }
});

//Listen for edge count from server
if (EDGE_RANK >= 0) {
    socket.on(`Process_${EDGE_RANK}`, (data) => {
        if (waitForResult.event == `Process_${EDGE_RANK}` && data.fromRank == waitForResult.fromRank) {
            waitForResult.result = data;
            waitForResult.event = '';
            waitForResult.arrived = true;
        }
        else {
            recvQueue.push(data);
            recvQueue.sort((a, b) => {
                b.time > a.time
            });
        }
    });

    socket.on(`ProcessBroadcast`, (data) => {
        if (waitForResult.event == `ProcessBroadcast`) {
            waitForResult.arrived = true;
            waitForResult.result = data;
            waitForResult.event = '';
        }
        else {
            recvQueue.push(data);
            recvQueue.sort((a, b) => {
                b.time > a.time
            });
        }
    });
}

function queueContains(rank) {
    for (var i = 0; i < recvQueue.length; i++) {
        if (recvQueue[i].fromRank == rank) { return true; }
    }

    return false;
}

function popQueue(rank) {
    var result = undefined;

    for (var i = recvQueue.length - 1; i >= 0; i--) {
        if (recvQueue[i].fromRank == rank) {
            result = recvQueue.splice(i, 1)[0];
            break;
        }
    }

    return result;
}

// Message Passing Functions //
function log(text) {
    $('#terminal').text(`${$('#terminal').text() + '[' + new Date().toLocaleTimeString() + '] ' + text + '\n'}`);
}

function MPI_Send(device_rank, data) {
    socket.emit(`Process_${device_rank}`, { fromRank: EDGE_RANK, data: data, toRank: device_rank, time: (new Date()).getTime() });
}

function MPI_Send_Broadcast(data) {
    socket.emit(`ProcessBroadcast`, { fromRank: '*', data: data, toRank: '*', time: (new Date()).getTime() });
}

async function MPI_Recv(fromRank) {
    if (queueContains(fromRank) == false) {
        //Setup object that will wait for data from server
        waitForResult.event = `Process_${EDGE_RANK}`;
        waitForResult.arrived = false;
        waitForResult.result = undefined;
        waitForResult.fromRank = fromRank;

        //Initlaise promise that will wait for data
        waitForResult.promise = new Promise(function (resolve, reject) {
            /* Wait for result from server */
            setInterval(function () {
                if (waitForResult.arrived) {
                    resolve(waitForResult.result.data);
                }
            }, 10);
        });

        return waitForResult.promise;
    }
    else {
        const result = popQueue(fromRank);
        console.log('Data already queued for rank ' + fromRank + ' ' + result);
        return result.data;
    }
}

async function MPI_Recv_Broadcast() {
    if (queueContains('*') == false) {
        //Setup object that will wait for data from server
        waitForResult.event = `ProcessBroadcast`;
        waitForResult.arrived = false;
        waitForResult.result = undefined;
        waitForResult.promise = undefined;
        waitForResult.fromRank = '*';

        //Initlaise promise that will wait for data
        waitForResult.promise = new Promise(function (resolve, reject) {
            /* Wait for result from server */
            setInterval(function () {
                if (waitForResult.arrived) {
                    resolve(waitForResult.result.data);
                }
            }, 10);
        });

        return waitForResult.promise;
    }
    else {
        return popQueue('*').data;
    }
}

function MPI_Edge_Rank() {
    return EDGE_RANK;
}

async function MPI_Edge_Count() {
    waitForResult.event = `Device_Count`;
    waitForResult.arrived = false;
    socket.emit(`Device_Count`, {});

    waitForResult.promise = new Promise(function (resolve, reject) {
        /* Wait for result from server */
        setInterval(function () {
            if (waitForResult.arrived) {
                resolve(waitForResult.result);
            }
        }, 10);
    });

    return waitForResult.promise;
}

function MPI_Download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

// function MPI_Chunk_Send(device_rank, data, chunkSize) {
//     const numberOfChunks = data.length / chunkSize;
//     log('data size is: ' + data.length);

//     if (Array.isArray(data)) {
//         log('Array -> Number of chunks to send: ' + numberOfChunks);
//         for (var i = 0; i < numberOfChunks; i++) {
//             const subData = data.splice(0, chunkSize > data.length ? data.length - 1 : chunkSize);
//             socket.emit(`Process_${device_rank}`, {
//                 fromRank: EDGE_RANK,
//                 data: subData,
//                 toRank: device_rank,
//                 time: (new Date()).getTime(),
//                 idx: i,
//                 maxIdx: numberOfChunks
//             });
//         }
//     }
//     // else {
//     //     data = data.toString();
//     //     log('String -> Number of chunks to send: ' + numberOfChunks);
//     //     for (var i = 0; i < data.length / chunkSize; i++) {
//     //         const chunkStart = i * chunkSize;
//     //         const chunkEnd = chunkStart + chunkSize;
//     //         const subData = data.substring(chunkStart, chunkEnd > data.length ? data.length - 1: chunkEnd);
//     //         socket.emit(`Process_${device_rank}`, {fromRank: EDGE_RANK, data: subData, toRank: device_rank, time: (new Date()).getTime(), idx: i});
//     //     }
//     // }
// }